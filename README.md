![banner](banner.png?raw=true)

## Contributing:
 - If you made changes to icons, or added a new one:
   - Delete the existing icon, if there is one
   - Run `cd assets; ./render-all.sh`
 - Create a pull request from your branch or fork
 - If any issues occur, report then to the [issue](https://github.com/vinceliuice/grub2-themes/issues) page

## Preview:
Images shown: tela/vimix/stylish/whitesur
![preview](preview.png?raw=true)

## Documents

[Grub2 theme reference](http://wiki.rosalab.ru/en/index.php/Grub2_theme_/_reference)

[Grub2 theme tutorial](http://wiki.rosalab.ru/en/index.php/Grub2_theme_tutorial)

## Roadmap

 * Discovery how licenses works in png and pf2 cases
 * Search how to use upstream tar.gz to generate and remove some files
 * Changes structure to generate 4 packages using package.install
